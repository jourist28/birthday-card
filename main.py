import tkinter as tk

def show_birthday_message():
    message = "Happy Birthday to my amazing gf! You are the best husband I could have ever hoped or dreamed to have. " \
              "I love you so much and every day am grateful that I get to spend the rest of my life with you! " \
              "For your birthday, I would like to gift you $100 in cash money for you to spend on whatever you like! " \
              "Maybe towards your bike funds? You get to decide. Happiest of birthdays! I love you!"
    label.config(text=message, wraplength=400)  # Adjust wraplength to fit the message

# Create the main window
root = tk.Tk()
root.title("Birthday Card")

# Create a label to display the message
label = tk.Label(root, text="", wraplength=400)  # Adjust wraplength to fit the message
label.pack(padx=20, pady=20)

# Create a button to show the message
button = tk.Button(root, text="Show Birthday Message", command=show_birthday_message)
button.pack(pady=10)

# Start the main event loop
root.mainloop()




